package org.cuatrovientos.java.loops;

import java.util.Scanner;

public class DrawSequence {
	private static Scanner sc;
	/*
	 * Ejercicio 3: Crea un proyecto con una clase llamada DrawSequence parecido al
	 * anterior pero la linea que debes mostrar debe tener este aspecto:
	 * *_*_*_*_*_* y siempre terminar en "*"
	 * Por ejemplo, si introduce el 5:
	  		Introduzca un numero entero mayor que cero:
			5
	 		*_*_*
	 * Por ejemplo, si introduce el 8:
	 		Introduzca un numero entero mayor que cero:
			8
	 		*_*_*_*_*	
	 */
	public static void ejercicio3() {
		int x;
		sc = new Scanner(System.in);
		System.out.println("Introduzca un numero entero mayor que cero:");
		x = Integer.parseInt(sc.nextLine());
		String cadena = "";
		boolean cambio = true;
		// Lo hacemos primero con un For
		System.out.println("Lo hacemos primero con un For");
		if (x > 0) {
			for (int i = 0; i < x; i++) {
				if (cambio) {
					cadena = cadena + "*";
					cambio = false;
				} else {
					cadena = cadena + "_";
					cambio = true;
				}
			}
			if (cambio) {
				cadena = cadena + "*";
			}
			System.out.println(cadena);
		} else {
			System.out.println("El mumero " + x + " introducido no es mayor que cero");
			return;
		}
		// Lo hacemos ahora con un While
		System.out.println("Lo hacemos ahora con un While");
		cadena = "";
		cambio = true;
		if (x > 0) {
			int i = 0;
			while (i < x) {
				if (cambio) {
					cadena = cadena + "*";
					cambio = false;
				} else {
					cadena = cadena + "_";
					cambio = true;
				}
				i++;
			}
			if (cambio) {
				cadena = cadena + "*";
			}
			System.out.println(cadena);
		} else {
			System.out.println("El mumero " + x + " introducido no es mayor que cero");
			return;
		}
		// Lo hacemos ahora con un Do-While
		System.out.println("Lo hacemos ahora con un Do-While");
		cadena = "";
		cambio = true;
		if (x > 0) {
			int i = 0;
			do {
				if (cambio) {
					cadena = cadena + "*";
					cambio = false;
				} else {
					cadena = cadena + "_";
					cambio = true;
				}
				i++;
			} while (i < x);
			if (cambio) {
				cadena = cadena + "*";
			}
			System.out.println(cadena);
		} else if (!(x % 2 == 0)) {
			System.out.println("El mumero " + x + " introducido es impar");
			return;
		} else {
			System.out.println("El mumero " + x + " introducido no es mayor que cero");
			return;
		}
		// Se nos fue la cabeza
		System.out.println("Se nos fue la cabeza");
		if (x > 0) {
			if (x % 2 == 0) {
				System.out.println(ejer3Recur(x, "", true) + "*");
			} else {
				System.out.println(ejer3Recur(x, "", true));
			}
		} else {
			System.out.println("El mumero " + x + " introducido no es mayor que cero");
			return;
		}
	}

	private static String ejer3Recur(int x, String cadena, boolean cambio) {
		if (x == 0) {
			return cadena;
		} else {
			if (cambio) {
				return ejer3Recur(x - 1, cadena + "*", false);
			} else {
				return ejer3Recur(x - 1, cadena + "_", true);
			}
		}

	}
}
