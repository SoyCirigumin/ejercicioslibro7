package org.cuatrovientos.java.loops;

import java.util.Scanner;

public class DrawSquare {
	/*
	 * Ejercicio 5: Crea un proyecto con una clase llamada DrawSquare que solicite
	 * al usuario un numero entero y usando ese valor debe "dibujar" en la consola
	 * un cuadrado formado por "*".
	 * Por ejemplo, si introduce el 4 se mostrara:
	   		****
			****
			****
			****
	 */

	private static Scanner sc;

	public static void ejercicio5() {
		int x;
		String cadena = "";
		sc = new Scanner(System.in);
		System.out.println("Introduzca un numero entero mayor que cero");
		x = Integer.parseInt(sc.nextLine());
		// Lo hacemos ahora con un Do-While y un for
		System.out.println("Lo hacemos ahora con un Do-While y un for");
		if (x > 0) {
			int i = 0;
			do {
				for (int j = 0; j < x; j++) {
					cadena = cadena + "*";
				}
				System.out.println(cadena);
				cadena = "";
				i++;
			} while (i < x);

		} else {
			System.out.println("El mumero " + x + " introducido no es mayor que cero");
			return;
		}

		// Lo hacemos con dos for
		System.out.println("Lo hacemos con dos for");
		if (x > 0) {
			for (int i = 0; i < x; i++) {
				for (int j = 0; j < x; j++) {
					cadena = cadena + "*";
				}
				System.out.println(cadena);
				cadena = "";
			}
		} else {
			System.out.println("El mumero " + x + " introducido no es mayor que cero");
			return;
		}

		// Lo hacemos ahora con un While y un Do-While
		System.out.println("Lo hacemos ahora con un While y un Do-While");
		if (x > 0) {
			int i = 0;
			while (i < x) {
				int j = 0;
				do {
					cadena = cadena + "*";
					j++;
				} while (j < x);
				System.out.println(cadena);
				cadena = "";
				i++;
			}

		} else {
			System.out.println("El mumero " + x + " introducido no es mayor que cero");
			return;
		}

		// Se nos fue la cabeza
		System.out.println("Se nos fue la cabeza");
		if (x > 0) {
			ejer5Recur(x, x, x, "");
		} else {
			System.out.println("El mumero " + x + " introducido no es mayor que cero");
			return;
		}

	}

	private static String ejer5Recur(int x, int y, int veces, String cadena) {
		if (x == 0) {
			return "";
		} else {
			if (y == 0) {
				System.out.println(cadena);
				return ejer5Recur(x - 1, veces, veces, "");
			} else {
				return ejer5Recur(x, y - 1, veces, cadena + "*");
			}

		}

	}

}
