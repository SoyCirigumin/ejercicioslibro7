package org.cuatrovientos.java.loops;

import java.util.Scanner;

public class Factorial {
	/*
	 * Ejercicio 6: Crea un proyecto con una clase llamada Factorial que solicite al
	 * usuario un numero entero y calcule su factorial. Por ejemplo, el factorial d
	 * 5 seria 5x4x3x2x1 = 120
	 */
	private static Scanner sc;

	public static void ejercicio6() {
		int x, resultado = 1;
		sc = new Scanner(System.in);
		System.out.println("Introduzca un numero entero mayor que cero");
		x = Integer.parseInt(sc.nextLine());
		// Lo hacemos ahora con un Do-While
		System.out.println("Lo hacemos ahora con un Do-While");
		if (x > 0) {
			int i = x;
			do {
				resultado = resultado * i;
				i--;
			} while (i > 0);
			System.out.println("El factorial del numero: " + x + " es: " + resultado);
		} else {
			System.out.println("El mumero " + x + " introducido no es mayor que cero");
			return;
		}
		// Lo hacemos con un for
		System.out.println("Lo hacemos con un for");
		if (x > 0) {
			resultado = 1;
			for (int i = x; i > 0; i--) {
				resultado = resultado * i;
			}
			System.out.println("El factorial del numero: " + x + " es: " + resultado);
		} else {
			System.out.println("El mumero " + x + " introducido no es mayor que cero");
			return;
		}
		// Lo hacemos ahora con un While
		System.out.println("Lo hacemos ahora con un While");
		if (x > 0) {
			int i = x;
			resultado = 1;
			while (i > 0) {
				resultado = resultado * i;
				i--;
			}
			System.out.println("El factorial del numero: " + x + " es: " + resultado);
		} else {
			System.out.println("El mumero " + x + " introducido no es mayor que cero");
			return;
		}

		// Se nos fue la cabeza
		System.out.println("Se nos fue la cabeza");
		if (x > 0) {
			System.out.println("El factorial del numero: " + x + " es: " + ejer6Recur(x, 1));
		} else {
			System.out.println("El mumero " + x + " introducido no es mayor que cero");
			return;
		}

	}

	private static int ejer6Recur(int x, int resultado) {
		if (x == 0) {
			return resultado;
		} else {
			return ejer6Recur(x - 1, resultado * x);
		}

	}

}
