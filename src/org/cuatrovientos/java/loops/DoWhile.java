package org.cuatrovientos.java.loops;

import java.util.Scanner;

public class DoWhile {
	/*
	 * * Ejercicio 4: Crea un proyecto con una clase llamada DoWhile que solicite al
	 * usuario una palabra y hasta que esta palabra no sea "out!" el programa no
	 * debe terminar y debe seguir solicitando una palabra
	 */
	private static final String OUT = "out!";
	private static Scanner sc;

	public static void ejercicio4() {
		String cadena;
		sc = new Scanner(System.in);
		// Lo hacemos ahora con un Do-While
		System.out.println("Lo hacemos ahora con un Do-While");
		do {
			System.out.println("Introduzca una palabra: out! para salir");
			cadena = sc.nextLine();
		} while (!cadena.equalsIgnoreCase(OUT));
		// Lo hacemos ahora con un While
		System.out.println("Lo hacemos ahora con un While");
		System.out.println("Introduzca una palabra: out! para salir");
		cadena = sc.nextLine();
		while (!cadena.equalsIgnoreCase(OUT)) {
			System.out.println("Introduzca una palabra: out! para salir");
			cadena = sc.nextLine();
		}
		// Se nos fue la cabeza
		System.out.println("Se nos fue la cabeza");
		System.out.println("Introduzca una palabra: out! para salir");
		ejer4Recur(sc.nextLine());
	}

	private static boolean ejer4Recur(String cadena) {
		if (cadena.equalsIgnoreCase(OUT)) {
			return true;
		} else {
			System.out.println("Introduzca una palabra: out! para salir");
			return ejer4Recur(sc.nextLine());
		}

	}
}
