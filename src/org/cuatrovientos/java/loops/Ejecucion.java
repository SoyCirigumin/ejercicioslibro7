package org.cuatrovientos.java.loops;

public class Ejecucion {

	public static void main(String[] args) {
		System.out.println("Ejercicio1:");
		SimpleLoop.ejercicio1();
		System.out.println("Ejercicio2:");
		DrawStars.ejercicio2();
		System.out.println("Ejercicio3:");
		DrawSequence.ejercicio3();
		System.out.println("Ejercicio4:");
		DoWhile.ejercicio4();
		System.out.println("Ejercicio5:");
		DrawSquare.ejercicio5();
		System.out.println("Ejercicio6:");
		Factorial.ejercicio6();
		System.out.println("Ejercicio7:");
		PrimeNumber.ejercicio7();
		System.out.println("Ejercicio8:");
		MultiplicationTable.ejercicio8();
	}

}
