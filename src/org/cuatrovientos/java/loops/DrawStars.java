package org.cuatrovientos.java.loops;

import java.util.Scanner;

public class DrawStars {
	/*
	 * Ejercicio 2: Crea un proyecto con una clase llamada DrawStars que solicite al
	 * usuario un valor entero, comprueba si es mayor que 0 y ademas par y si es asi muestre por
	 * pantalla una linea con el caracter "*" (asterisco) tantas veces como el valor
	 * del numero. Usa System.Out.Print("*"); Por ejemplo, si introduce el 8
	 * mostrara: ******** 
	 * Si el valor introducido no cumple los requisitos debes
	 * mostrar un mensaje de advertencia al usuario y terminar el programa con
	 * return; o con System.exit(0);
	 */
	private static Scanner sc;

	public static void ejercicio2() {
		int x;
		sc = new Scanner(System.in);
		System.out.println("Introduzca un numero entero mayor que cero:");
		x = Integer.parseInt(sc.nextLine());
		String cadena = "";
		// Lo hacemos primero con un For
		System.out.println("Lo hacemos primero con un For");
		if (x > 0 && x % 2 == 0) {
			for (int i = 0; i < x; i++) {
				cadena = cadena + "*";
			}
			System.out.println(cadena);
		} else if (!(x % 2 == 0)) {
			System.out.println("El mumero " + x + " introducido es impar");
			return;
		} else {
			System.out.println("El mumero " + x + " introducido no es mayor que cero");
			return;
		}
		
		// Lo hacemos ahora con un While
		System.out.println("Lo hacemos ahora con un While");
		cadena = "";
		if (x > 0 && x % 2 == 0) {
			int i = 0;
			while (i < x) {
				cadena = cadena + "*";
				i++;
			}
			System.out.println(cadena);
		} else if (!(x % 2 == 0)) {
			System.out.println("El mumero " + x + " introducido es impar");
			return;
		} else {
			System.out.println("El mumero " + x + " introducido no es mayor que cero");
			return;
		}
		
		// Lo hacemos ahora con un Do-While
		System.out.println("Lo hacemos ahora con un Do-While");
		cadena = "";
		if (x > 0 && x % 2 == 0) {
			int i = 0;
			do {
				cadena = cadena + "*";
				i++;
			} while (i < x);
			System.out.println(cadena);
		} else if (!(x % 2 == 0)) {
			System.out.println("El mumero " + x + " introducido es impar");
			return;
		} else {
			System.out.println("El mumero " + x + " introducido no es mayor que cero");
			return;
		}
		
		// Se nos fue la cabeza
		System.out.println("Se nos fue la cabeza");
		if (x > 0 && x % 2 == 0) {
			System.out.println(ejer2Recur(x, ""));
		} else if (!(x % 2 == 0)) {
			System.out.println("El mumero " + x + " introducido es impar");
			return;
		} else {
			System.out.println("El mumero " + x + " introducido no es mayor que cero");
			return;
		}
	}

	private static String ejer2Recur(int x, String cadena) {
		if (x == 0) {
			return cadena;
		} else {
			return ejer2Recur(x - 1, cadena + "*");
		}

	}
}
