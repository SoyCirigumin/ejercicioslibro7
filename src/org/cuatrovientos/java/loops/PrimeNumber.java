package org.cuatrovientos.java.loops;

import java.util.Scanner;

public class PrimeNumber {
	/*
	 * Ejercicio 7: Crea un proyecto con una clase llamada PrimeNumber que solicite
	 * al usuario un numero entero y compruebe si ese numero es primo o no, es decir
	 * si solamente es divisible por si mismo o por 1.
	 */
	private static Scanner sc;

	public static void ejercicio7() {
		int x;
		int contador = 2;
		sc = new Scanner(System.in);
		System.out.println("Introduzca un numero entero mayor que cero");
		x = Integer.parseInt(sc.nextLine());
		boolean primo = true;
		if (x % 2 == 0 || x == 1) {
			primo = false;
		}
		while ((primo) && (contador != x)) {
			if (x % contador == 0)
				primo = false;
			contador++;
		}
		if (primo || x == 1) {
			System.out.println("El numero " + x + " es un numero primo");
		} else {
			System.out.println("El numero " + x + " no es un numero primo");
		}
	}
}
