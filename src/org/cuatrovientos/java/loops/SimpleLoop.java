package org.cuatrovientos.java.loops;

import java.util.Scanner;

public class SimpleLoop {
	/*
	 * Ejercicio 1: Crea un proyecto con una clase llamada SimpleLoop que solicite
	 * al usuario un valor entero, comprueba si es mayor que 0 y si es asi muestre
	 * por pantalla un saludo tantas veces como el valor del numero. Si el valor
	 * introducido no es mayor que 0 debes mostrar un mensaje de advertencia al
	 * usuario y terminar el programa con return; o con System.exit(0);
	 		Introduzca un numero entero mayor que cero:
			4
			Hello!
			Hello!
			Hello!
			Hello!
	 */
	private static Scanner sc;

	public static void ejercicio1() {
		int x;
		sc = new Scanner(System.in);
		System.out.println("Introduzca un numero entero mayor que cero:");
		x = Integer.parseInt(sc.nextLine());
		// Lo hacemos primero con un For
		System.out.println("Lo hacemos primero con un For");
		if (x > 0) {
			for (int i = 0; i < x; i++) {
				System.out.println("Hello!");
			}
		} else {
			System.out.println("El mumero " + x + " introducido no es mayor que cero");
			return;
		}

		// Lo hacemos ahora con un While
		System.out.println("Lo hacemos ahora con un While");
		if (x > 0) {
			int i = 0;
			while (i < x) {
				System.out.println("Hello!");
				i++;
			}
		} else {
			System.out.println("El mumero " + x + " introducido no es mayor que cero");
			return;
		}

		// Lo hacemos ahora con un Do-While
		System.out.println("Lo hacemos ahora con un Do-While");
		if (x > 0) {
			int i = 0;
			do {
				System.out.println("Hello!");
				i++;
			} while (i < x);
		} else {
			System.out.println("El mumero " + x + " introducido no es mayor que cero");
			return;
		}

		// Se nos fue la cabeza
		System.out.println("Se nos fue la cabeza");
		if (x > 0) {
			ejer1Recur(x);
		} else {
			System.out.println("El mumero " + x + " introducido no es mayor que cero");
			return;
		}

	}

	private static int ejer1Recur(int x) {
		if (x == 0) {
			return 0;
		} else {
			System.out.println("Hello!");
			return ejer1Recur(x - 1);
		}

	}
}
