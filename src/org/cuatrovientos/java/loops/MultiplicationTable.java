package org.cuatrovientos.java.loops;

public class MultiplicationTable {
	/*
	 * Ejercicio 8: Crea un proyecto con una clase llamada MultiplicationTable que
	 * muestre todas las tablas de multiplicar desde el numero 0 al 10
	 */
	public static void ejercicio8() {
		String cadena = "";
		// Lo hacemos ahora con un Do-While y un for
		System.out.println("Lo hacemos ahora con un Do-While y un for");
		int i = 0;
		do {
			System.out.println("Multiplication table for " + i);
			for (int j = 0; j <= 10; j++) {
				cadena = cadena + i + " x " + j + " = " + i * j + " | ";
			}
			System.out.println(cadena);
			cadena = "";
			i++;
		} while (i <= 10);

		// Lo hacemos con dos for
		System.out.println("Lo hacemos con dos for");
		for (i = 0; i <= 10; i++) {
			System.out.println("Multiplication table for " + i);
			for (int j = 0; j <= 10; j++) {
				cadena = cadena + i + " x " + j + " = " + i * j + " | ";
			}
			System.out.println(cadena);
			cadena = "";
		}

		// Lo hacemos ahora con un While y un Do-While
		System.out.println("Lo hacemos ahora con un While y un Do-While");
		i = 0;
		while (i <= 10) {
			System.out.println("Multiplication table for " + i);
			int j = 0;
			do {
				cadena = cadena + i + " x " + j + " = " + i * j + " | ";
				j++;
			} while (j <= 10);
			System.out.println(cadena);
			cadena = "";
			i++;
		}

		// Se nos fue la cabeza
		System.out.println("Se nos fue la cabeza");
		ejer6Recur(10, 10, "");
	}

	private static String ejer6Recur(int x, int y, String cadena) {
		if (x < 0) {
			return "";
		} else {
			if (y < 0) {
				System.out.println("Multiplication table for " + x);
				System.out.println(cadena);
				return ejer6Recur(x - 1, 10, "");
			} else {
				cadena = cadena + x + " x " + y + " = " + x * y + " | ";
				return ejer6Recur(x, y - 1, cadena);
			}

		}

	}
}
